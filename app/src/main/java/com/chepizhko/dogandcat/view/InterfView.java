package com.chepizhko.dogandcat.view;

import com.chepizhko.dogandcat.model.ImageItem;

import java.util.List;

public interface InterfView {
    void setItem(List<ImageItem> item);
}
