package com.chepizhko.dogandcat.view;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chepizhko.dogandcat.presenter.MainPresenter;
import com.chepizhko.dogandcat.R;
import com.chepizhko.dogandcat.adapter.CatAdapter;
import com.chepizhko.dogandcat.model.ImageItem;

import java.util.ArrayList;
import java.util.List;

public class DogFragment extends Fragment implements InterfView {
    private MainPresenter mPresenter;
    private List<ImageItem> items = new ArrayList<>();
    private CatAdapter mCatAdapter;
    public DogFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new MainPresenter(this);
        mPresenter.onViewCreated("dog");
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tabs2_dog, container, false);
        RecyclerView mPhotoRecyclerView = (RecyclerView) rootView.findViewById(R.id.dog_recycler_view);
        mPhotoRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCatAdapter = new CatAdapter(getContext(), items);
        mPhotoRecyclerView.setAdapter(mCatAdapter);
        return rootView;
    }
    @Override
    public void setItem(List<ImageItem> item) {
        items = item;
        mCatAdapter.setData(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDestroy();
    }
}
