package com.chepizhko.dogandcat;

import android.os.AsyncTask;

import com.chepizhko.dogandcat.data.AppDatabase;
import com.chepizhko.dogandcat.data.Cat;
import com.chepizhko.dogandcat.data.CatDao;
import com.chepizhko.dogandcat.data.Dog;
import com.chepizhko.dogandcat.data.DogDao;
import com.chepizhko.dogandcat.model.ImageItem;

import java.util.List;
/*
* Класс загружает в базу данные.
* */
public class SaveDataTask extends AsyncTask<Void, Void, List<ImageItem>> {
    private List<ImageItem> imageItems;
    private String animal;

    public SaveDataTask(List<ImageItem> items, String anim) {
        imageItems = items;
        animal = anim;
    }

    // метод doInBackground(…) для для заполнения базы
    @Override
    protected List<ImageItem> doInBackground(Void... params) {
        // получение базы
        AppDatabase db = App.getInstance().getDatabase();
        // Из Database объекта получаем Dao
        if (animal.equals("cat")) {
            CatDao catDao = db.catDao();
            Cat cat = new Cat();
            for (ImageItem item : imageItems) {
                cat.title = item.getTitle();
                cat.url = item.getUrl();
                catDao.insert(cat);
            }
        }
        if (animal.equals("dog")) {
            DogDao dogDao = db.dogDao();
            Dog dog = new Dog();
            for (ImageItem item : imageItems) {
                dog.title = item.getTitle();
                dog.url = item.getUrl();
                dogDao.insert(dog);
            }
        }
        return imageItems;
    }
    @Override
    protected void onPostExecute(List<ImageItem> items) {
    }
}
