package com.chepizhko.dogandcat.network;

import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class CheckNetwork {

    public static boolean netIsAvailable(){
        String TAG = "myLogs";
        HttpURLConnection connection = null;
        try {
            URL url=new URL("http://www.google.com");
            connection=(HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setReadTimeout(1000);
            connection.connect();
            // статус ресурса OK
            if (connection.getResponseCode() == 200) {
                return true;
            }else{ return false; }
            // иначе проверка провалилась
        }catch (ClassCastException | IOException e){
            Log.d(TAG, "Ошибка проверки подключения к интернету", e);
            return false;
        } finally {
            releaseResource(connection);
        }
    }
    private static void releaseResource(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }

}

