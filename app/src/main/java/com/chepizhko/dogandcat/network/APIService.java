package com.chepizhko.dogandcat.network;

import com.chepizhko.dogandcat.model.Animals;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("xim/api.php?")
    Call<Animals> callBack(@Query("query") String query);

}