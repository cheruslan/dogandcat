package com.chepizhko.dogandcat.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Cat {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public String title;

    public String url;

}
