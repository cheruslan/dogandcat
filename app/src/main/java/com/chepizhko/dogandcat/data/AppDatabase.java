package com.chepizhko.dogandcat.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Cat.class, Dog.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CatDao catDao();
    public abstract DogDao dogDao();
}
