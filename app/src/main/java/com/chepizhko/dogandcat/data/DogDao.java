package com.chepizhko.dogandcat.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface DogDao {
    // позволяют получить полный список котов
    @Query("SELECT * FROM dog")
    List<Dog> getAll();
    // позволяют получить конкретного кота по id.
    @Query("SELECT * FROM dog WHERE title = :id")
    Dog getByTitle(String id);

    @Insert
    void insert(Dog dog);

    @Update
    void update(Dog dog);

    @Delete
    void delete(Dog dog);
}
