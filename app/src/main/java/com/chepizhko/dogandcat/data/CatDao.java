package com.chepizhko.dogandcat.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface CatDao {
    // позволяют получить полный список котов
    @Query("SELECT * FROM cat")
    List<Cat> getAll();
    // позволяют получить конкретного кота по id.
    @Query("SELECT * FROM cat WHERE title = :id")
    Cat getByTitle(String id);

    @Insert
    void insert(Cat cat);

    @Update
    void update(Cat cat);

    @Delete
    void delete(Cat cat);
}
