package com.chepizhko.dogandcat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chepizhko.dogandcat.App;
import com.chepizhko.dogandcat.R;
import com.chepizhko.dogandcat.model.ImageItem;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CatAdapter extends RecyclerView.Adapter<CatAdapter.CatViewHolder> {

    private Context mContext;
    private List<ImageItem> items;

    public class CatViewHolder extends RecyclerView.ViewHolder {
        private ImageView url_image;
        private TextView title;
        private LinearLayout mLayout;
        Picasso picasso = new Picasso.Builder(mContext)
                .downloader(new OkHttp3Downloader(App.getClient())).build();

        CatViewHolder(View itemView) {
            super(itemView);
            url_image = (ImageView) itemView.findViewById(R.id.id_url);
            title = (TextView)itemView.findViewById(R.id.id_title);
            mLayout = (LinearLayout)itemView.findViewById(R.id.linear_item);
        }

        private void bindGalleryItem(ImageItem imageItem) {
            title.setText(imageItem.getTitle());

            picasso.load(imageItem.getUrl()).
                    resize(70, 80).
                    into(url_image);
        }
    }

    public CatAdapter(Context context, List<ImageItem> items){
        this.mContext = context;
        this.items = items;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public CatViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_item, viewGroup, false);
        CatViewHolder pvh = new CatViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(CatViewHolder prestaViewHolder, @SuppressLint("RecyclerView") int i) {
        final ImageItem imageItem = items.get(i);
        prestaViewHolder.bindGalleryItem(imageItem);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<ImageItem> items){
        this.items = items;
        notifyDataSetChanged();
    }
}

