package com.chepizhko.dogandcat.model;

import com.google.gson.annotations.SerializedName;

public class ImageItem {
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String url;

    public ImageItem(String title, String url_image) {
        this.title = title;
        this.url = url_image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}