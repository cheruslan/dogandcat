package com.chepizhko.dogandcat.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Animals {
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ArrayList<ImageItem> data;

    public ArrayList<ImageItem> getData() {
        return data;
    }

    public void setData(ArrayList<ImageItem> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
