package com.chepizhko.dogandcat.model;

import java.util.List;

public interface RepoModel {

    void loadData(String str);
    List<ImageItem> getItemsCat();
    List<ImageItem> getItemsDog();

}
