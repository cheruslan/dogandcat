package com.chepizhko.dogandcat.model;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.chepizhko.dogandcat.App;
import com.chepizhko.dogandcat.SaveDataTask;
import com.chepizhko.dogandcat.data.AppDatabase;
import com.chepizhko.dogandcat.data.Cat;
import com.chepizhko.dogandcat.data.Dog;
import com.chepizhko.dogandcat.network.CheckNetwork;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Respository implements RepoModel {

    private List<ImageItem> mItemsCat = new ArrayList<>();
    private List<ImageItem> mItemsDog = new ArrayList<>();
    private String TAG = "myLogs";
    private boolean isNetwork;

    @Override
    public void loadData(String animal) {
        new ProgressTask(animal).execute();
    }

    /*
     * Метод делает запрос, получает response, создаёт список объектов;
     * */
    synchronized private void setRequest(final String cat_or_dog) {
        // get resp
        Call<Animals> resp = App.getService().callBack(cat_or_dog);
        resp.enqueue(new Callback<Animals>() {
            @Override
            public void onResponse(@NonNull Call<Animals> call, @NonNull Response<Animals> response) {
                Log.d(TAG, "RESPONSE 234================== " + response);

                if (response.isSuccessful()) {
                    if (cat_or_dog.equals("cat")) {
                        mItemsCat = response.body().getData();
                        // сохраняем в базу
                        new SaveDataTask(mItemsCat, cat_or_dog).execute();
                    }
                    if (cat_or_dog.equals("dog")) {
                        mItemsDog = response.body().getData();
                        // сохраняем в базу
                        new SaveDataTask(mItemsDog, cat_or_dog).execute();
                    }
                } else {
                    Log.d(TAG, "RESPONSE FILED ");
                }
            }

            @Override
            public void onFailure(@NonNull Call<Animals> call, Throwable t) {
                Log.d(TAG, "RESPONSE ERROR ");
            }
        });
    }

    @Override
    public List<ImageItem> getItemsCat() {
        return mItemsCat;
    }

    @Override
    public List<ImageItem> getItemsDog() {
        return mItemsDog;
    }

    // Поток проверяет на соединение и определяет запрос в сеть или в базу.
    public class ProgressTask extends AsyncTask<String, Void, String> {
        String animal;

        ProgressTask(String anim) {
            animal = anim;
        }

        @Override
        protected String doInBackground(String... path) {
            // проверка на подключение
            isNetwork = CheckNetwork.netIsAvailable();
            Log.d(TAG, "ПРОВЕРКА СЕТИ = " + isNetwork);
            if (isNetwork) {
                Log.d(TAG, "ЗАПРОС НА СЕРВЕР");
                setRequest(animal);
            } else {
                Log.d(TAG, "ЗАГРУЗКА С БАЗЫ");
                new LoadDataTask(animal).execute();
            }

            return null;
        }
    }
    /*
    * Класс загружает список из базы, если нет подключения
    * */
    public class LoadDataTask extends AsyncTask<Void, Void, List<ImageItem>> {

        private String animal;

        public LoadDataTask(String anim) {
            animal = anim;
        }
        // метод doInBackground(…) для запроса в базу
        @Override
        protected List<ImageItem> doInBackground(Void... params) {
            // получение базы
            AppDatabase db = App.getInstance().getDatabase();
            if (animal.equals("cat")) {
                List<Cat> lstCat = db.catDao().getAll();
                for (int j = 0; j < lstCat.size(); j++) {
                    mItemsCat.add(new ImageItem(lstCat.get(j).title, lstCat.get(j).url));
                }
            }
            if (animal.equals("dog")) {
                List<Dog> lstDog = db.dogDao().getAll();
                for (int j = 0; j < lstDog.size(); j++) {
                    mItemsDog.add(new ImageItem(lstDog.get(j).title, lstDog.get(j).url));
                }
            }
            return null;
        }
    }
}



