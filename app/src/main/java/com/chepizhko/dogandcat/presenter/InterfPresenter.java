package com.chepizhko.dogandcat.presenter;

public interface InterfPresenter {
    void onViewCreated(String str);
    void onDestroy();
}
