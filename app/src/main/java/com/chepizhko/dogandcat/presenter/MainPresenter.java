package com.chepizhko.dogandcat.presenter;

import android.os.Handler;

import com.chepizhko.dogandcat.model.ImageItem;
import com.chepizhko.dogandcat.model.RepoModel;
import com.chepizhko.dogandcat.model.Respository;
import com.chepizhko.dogandcat.view.InterfView;

import java.util.ArrayList;
import java.util.List;

public class MainPresenter implements InterfPresenter {
    private String TAG = "myLogs";
    private InterfView mInterfView;
    private RepoModel mRepoModel;
    private List<ImageItem> mItemsCat = new ArrayList<>();
    private List<ImageItem> mItemsDog = new ArrayList<>();


    public MainPresenter(InterfView interfView){
        mInterfView = interfView;
        mRepoModel = new Respository();
    }
    // метод запрашивает данные в репозитории и отправляет во фрагмент
    @Override
    public void onViewCreated(final String animal) {
        mRepoModel.loadData(animal);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(animal.equals("cat")) {
                    mItemsCat = mRepoModel.getItemsCat();
                    mInterfView.setItem(mItemsCat);
                }
                if(animal.equals("dog")) {
                    mItemsDog = mRepoModel.getItemsDog();
                    mInterfView.setItem(mItemsDog);
                }
            }
        },700);
    }

    @Override
    public void onDestroy() {

    }
}
